#include <stdio.h>
#include <ctime>
#include <iostream>
#include <time.h>
#include <string>

#if defined _MSC_VER
	#include <direct.h>
#elif defined __GNUC__
	#include <sys/types.h>
	#include <sys/stat.h>
#endif



void fetchImages(char celestialBody, char* date) {
	// Create folder for each date
	std::string foldername(celestialBody + date);
	#if defined _MSC_VER
		_mkdir(foldername.data());
	#elif defined __GNUC__
		mkdir(foldername.data(), 0777);
	#endif
	// Go to website
	// Fetch images and authors
	// Save images to folder (filename: author)
}

int main(int argc, char** argv) {

	// Checks size of arguments
	if (argc <= 2) {
		std::cerr << "Usage: ALPOJFetcher <planet [mercury | venus | mars | jupiter | saturn | uranus | neptune | pluto | other]> <startdate [YYYY-MM-DD]> <enddate [YYYY-MM-DD] (optional)>" << std::endl;
		std::cerr << "Example: ALPOJFetcher jupiter 2015-02-08" << std::endl;
		return 0;
	}
	
	// Get current time
	std::time_t now = std::time(NULL);
	
	// Create time element from start date
	char* start = argv[2];
	struct tm tmStart;
	strptime(start, "%Y-%m-%d", &tmStart);
	std::time_t startdate = std::mktime(&tmStart);
	
	// Check if there is a fourth argument:
	// If there isn't the start and end times are the same, if there is, they are different
	char* end;
	if (argc == 3)
		end = argv[3];
	else
		end = argv[2];
		
	// Create time element from end date
	struct tm tmEnd;
	strptime(end, "%Y-%m-%d", &tmEnd);
	std::time_t enddate = std::mktime(&tmEnd);
	
	// Check if dates are not in the future
	if (std::difftime(now, startdate) < 0 || std::difftime(enddate, now) > 0) {
		std::cerr << "Error: can't travel to the future!" << std::endl;
		return 0;
	}
	
	// Get initial day and for each day, fetch the images from the corresponding celestial body
	struct tm st = tmStart;
	do {
		char current[100];
		std::time_t currentDate = std::mktime(&st);
		std::strftime(current, sizeof(current), "%Y%m%d", std::localtime(&currentDate));
		char celestialBody = argv[1][0];
		fetchImages(celestialBody, current);
		st.tm_mday++;
	} while (std::difftime(enddate, startdate) != 0);
	
	return 0;
}